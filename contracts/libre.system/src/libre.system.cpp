#include <libre.system/libre.system.hpp>
#include <eosio.token/eosio.token.hpp>
#include <cmath>

namespace libresystem
{

   using eosio::current_time_point;
   using eosio::token;

   system_contract::system_contract(name s, name code, datastream<const char *> ds)
       : native(s, code, ds),
         _payments(get_self(), get_self().value),
         _voters(get_self(), get_self().value),
         _producers(get_self(), get_self().value),
         _global(get_self(), get_self().value),
         _global2(get_self(), get_self().value),
         _migrate(get_self(), get_self().value),
         _rammarket(get_self(), get_self().value)
   {
      _gstate = _global.exists() ? _global.get() : get_default_parameters();
      _gstate2 = _global2.exists() ? _global2.get() : eosio_global_state2{};
   }

   eosio_global_state system_contract::get_default_parameters()
   {
      eosio_global_state dp;
      get_blockchain_parameters(dp);
      return dp;
   }

   symbol system_contract::core_symbol() const
   {
      const static auto sym = get_core_symbol(_rammarket);
      return sym;
   }

   system_contract::~system_contract()
   {
      _global.set(_gstate, get_self());
      _global2.set(_gstate2, get_self());
   }

   void system_contract::setparams(const eosio::blockchain_parameters &params)
   {
      require_auth(get_self());
      set_blockchain_parameters(params);
   }

   void system_contract::setpriv(const name &account, uint8_t ispriv)
   {
      require_auth(get_self());
      set_privileged(account, ispriv);
   }

   void system_contract::setalimits(const name &authorizer, const name &account, int64_t ram, int64_t net, int64_t cpu)
   {
      require_auth(authorizer);
      check(checkPermission(authorizer, "setalimits") == 1, "You are not authorised to set limits");

      set_resource_limits(account, ram, net, cpu);
   }

   void system_contract::activate(const eosio::checksum256 &feature_digest)
   {
      require_auth(get_self());
      preactivate_feature(feature_digest);
   }

   void system_contract::rmvproducer(const name &producer)
   {
      require_auth(get_self());
      auto prod = _producers.find(producer.value);
      check(prod != _producers.end(), "producer not found");
      _producers.modify(prod, same_payer, [&](auto &p)
                        { p.deactivate(); });
   }

   void system_contract::migrate(uint32_t max_steps)
   {
      if (!_migrate.exists())
      {
         _migrate.set(migrate_state{}, get_self());
      }

      eosio::check(_migrate.get().state < e_migrate_state::MIGRATION_CONCLUDED, "Migration is already complete");

      if (_migrate.get().state == e_migrate_state::MIGRATE_VOTERS)
      {
         max_steps = migrate_voters(max_steps);
      }

      if (_migrate.get().state == e_migrate_state::MIGRATE_PRODUCERS)
      {
         max_steps = migrate_producers(max_steps);
      }

      if (_migrate.get().state == e_migrate_state::MIGRATE_TOTAL_PRODUCER_VOTE_WEIGHT)
      {
         migrate_total_producer_vote_weight(max_steps);
      }
   }

   uint32_t system_contract::migrate_voters(uint32_t max_steps)
   {
      auto migration = _migrate.get();
      bool is_voter_starting = migration.voter == eosio::name{};
      auto voter_itr = is_voter_starting ? _voters.begin() : _voters.find(migration.voter.value);

      for (; voter_itr != _voters.end() && max_steps > 0; --max_steps, ++voter_itr)
      {
         _voters.modify(voter_itr, eosio::same_payer, [&](auto &row)
                        { row.staked = get_voting_power(voter_itr->owner); });
      }

      if (voter_itr == _voters.end())
      {
         migration.voter = eosio::name{};
         migration.state = e_migrate_state::MIGRATE_PRODUCERS; // pass to migrate producers
      }
      else
      {
         migration.voter = voter_itr->owner;
      }

      _migrate.set(migration, get_self());

      return max_steps;
   }

   uint32_t system_contract::migrate_producers(uint32_t max_steps)
   {
      auto migration = _migrate.get();
      bool is_producer_starting = migration.producer == eosio::name{};
      auto producer_itr = is_producer_starting ? _producers.begin() : _producers.find(migration.producer.value);

      for (; producer_itr != _producers.end() && max_steps > 0;)
      {
         auto is_voter_starting = migration.voter == eosio::name{};
         int64_t total_votes = is_voter_starting ? 0 : producer_itr->total_votes;
         auto voter_itr = is_voter_starting ? _voters.begin() : _voters.find(migration.voter.value);

         for (; voter_itr != _voters.end() && max_steps > 0; --max_steps, ++voter_itr)
         {
            auto container = voter_itr->producers;

            if (std::find(container.begin(), container.end(), producer_itr->owner) != container.end())
            {
               total_votes += get_voting_power(voter_itr->owner);
            }
         }

         _producers.modify(producer_itr, eosio::same_payer, [&](auto &row)
                           { row.total_votes = total_votes; });

         bool has_voter_finished = voter_itr == _voters.end();

         if (has_voter_finished)
         {
            ++producer_itr;
            migration.voter = eosio::name{};
         }
         else
         {
            migration.voter = voter_itr->owner;
         }
      }

      bool has_migration_finished = producer_itr == _producers.end();

      if (has_migration_finished)
      {
         migration.producer = eosio::name(-1);
         migration.state = e_migrate_state::MIGRATE_TOTAL_PRODUCER_VOTE_WEIGHT;
      }
      else
      {
         migration.producer = producer_itr->owner;
      }
      _migrate.set(migration, get_self());

      return max_steps;
   }

   void system_contract::migrate_total_producer_vote_weight(uint32_t max_steps)
   {
      auto migration = _migrate.get();
      bool is_voter_starting = migration.voter == eosio::name{};
      auto voter_itr = is_voter_starting ? _voters.begin() : _voters.find(migration.voter.value);
      double producer_votes = is_voter_starting ? 0 : _gstate.total_producer_vote_weight;

      for (; voter_itr != _voters.end() && max_steps > 0; --max_steps, ++voter_itr)
      {
         producer_votes += voter_itr->staked;
      }

      if (voter_itr == _voters.end())
      {
         migration.voter = eosio::name{};
         migration.state = e_migrate_state::MIGRATION_CONCLUDED; // finish migration
      }
      else
      {
         migration.voter = voter_itr->owner;
      }

      _gstate.total_producer_vote_weight = producer_votes;

      _migrate.set(migration, get_self());
   }

   /**
    *  Called after a new account is created. This code enforces resource-limits rules
    *  for new accounts as well as new account naming conventions.
    *
    *  Account names containing '.' symbols must have a suffix equal to the name of the creator.
    *  This allows users who buy a premium name (shorter than 12 characters with no dots) to be the only ones
    *  who can create accounts with the creator's name as a suffix.
    *
    */
   void native::newaccount(const name &creator,
                           const name &newact,
                           ignore<authority> owner,
                           ignore<authority> active)
   {

      bool isPrivileged = creator == get_self() || creator == "libre"_n;

      check(isPrivileged || system_contract::checkPermission(creator, "createacc") == 1, "You are not authorised to create accounts");

      if (!isPrivileged)
      { // bypass checks if creator is eosio or libre
         uint64_t tmp = newact.value >> 4;
         bool has_dot = false;

         for (uint32_t i = 0; i < 12; ++i)
         {
            has_dot |= !(tmp & 0x1f);
            tmp >>= 5;
         }

         if (has_dot)
         {
            name suffix = newact.suffix();
            bool has_dot = suffix != newact;
            if (has_dot)
            {
               check(creator == suffix, "Only suffix may create accounts that use suffix");
            }
         }
      }

      check(system_contract::checkPermission(creator, "setalimits") == 1, "You are not authorised to set limits");

      set_resource_limits(newact, 5120, 1, 1);
   }

   void native::setabi(const name &acnt, const std::vector<char> &abi)
   {
      eosio::multi_index<"abihash"_n, abi_hash> table(get_self(), get_self().value);
      auto itr = table.find(acnt.value);
      if (itr == table.end())
      {
         table.emplace(acnt, [&](auto &row)
                       {
            row.owner = acnt;
            row.hash = eosio::sha256(const_cast<char*>(abi.data()), abi.size()); });
      }
      else
      {
         table.modify(itr, same_payer, [&](auto &row)
                      { row.hash = eosio::sha256(const_cast<char *>(abi.data()), abi.size()); });
      }
   }

   void system_contract::init(unsigned_int version, const symbol &core)
   {
      require_auth(get_self());

      check(version.value == 0, "unsupported version for init action");

      auto itr = _rammarket.find(ramcore_symbol.raw());
      check(itr == _rammarket.end(), "system contract has already been initialized");

      auto system_token_supply = eosio::token::get_supply(token_account, core.code());
      check(system_token_supply.symbol == core, "specified core symbol does not exist (precision mismatch)");

      check(system_token_supply.amount > 0, "system token supply must be greater than 0");
      _rammarket.emplace(get_self(), [&](auto &m)
                         {
         m.supply.amount = 100000000000000ll;
         m.supply.symbol = ramcore_symbol;
         m.base.balance.amount = int64_t(_gstate.free_ram());
         m.base.balance.symbol = ram_symbol;
         m.quote.balance.amount = system_token_supply.amount / 1000;
         m.quote.balance.symbol = core; });

      token::open_action open_act{token_account, {{get_self(), active_permission}}};
      open_act.send(rex_account, core, get_self());
   }

   /* ------------ LIBRE CODE ------------- */
   uint8_t system_contract::checkPermission(name acc, std::string permission)
   {
      const std::map<std::string, uint8_t> accperm = eosio::eosiolibre::get_priv(system_contract::libre_account, acc);
      auto permch = accperm.find(permission);
      return permch->second;
   }

} /// libre.system
